package com.extweb4j.web.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.extweb4j.core.model.ExtMenu;

public class MenuBean {
	private String id;
	private String text;
	private boolean leaf;
	private boolean expanded = false;
	private String pid;
	private String viewType;
	private Date create_time;
	private String iconCls;
	private String rowCls;
	private String deep;
	private int sort;
	private String action;
	private String status;
	
	private List<MenuBean> children = new ArrayList<MenuBean>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isLeaf() {
		return leaf;
	}
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public String getRowCls() {
		return rowCls;
	}
	public void setRowCls(String rowCls) {
		this.rowCls = rowCls;
	}
	public String getDeep() {
		return deep;
	}
	public void setDeep(String deep) {
		this.deep = deep;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<MenuBean> getChildren() {
		return children;
	}
	public void setChildren(List<MenuBean> children) {
		this.children = children;
	}
	public MenuBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public boolean isExpanded() {
		return expanded;
	}
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	public MenuBean(ExtMenu extMenu) {
		super();
		// TODO Auto-generated constructor stub
		this.setId(extMenu.getStr("id"));
		this.setAction(extMenu.getStr("action"));
		this.setCreate_time(extMenu.getDate("create_time"));
		this.setDeep(extMenu.getStr("deep"));
		this.setIconCls(extMenu.getStr("icon_cls"));
		String leaf = extMenu.getStr("leaf");
		if(StringUtils.isNotBlank(leaf) && leaf.equals("1")){
			this.setLeaf(true);
		}else{
			this.setLeaf(false);
		}
		this.setPid(extMenu.getStr("pid"));
		this.setRowCls(extMenu.getStr("row_cls"));
		this.setSort(extMenu.getInt("sort"));
		this.setStatus(extMenu.getStr("status"));
		this.setText(extMenu.getStr("text"));
		this.setViewType(extMenu.getStr("view_type"));
	}
	/**
	 * 将数据库菜单Model转换为Bean
	 * @param list
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<MenuBean> covertExtMenu(List<ExtMenu> list) {
		// TODO Auto-generated method stub
		List<MenuBean> menuBeans = new ArrayList<MenuBean>();
		for(ExtMenu menu : list){
			MenuBean bean = new MenuBean(menu);
			List<MenuBean> menuBeans2 = new ArrayList<MenuBean>();
			Object object = menu.get("children");
			if(object!=null){
				for(ExtMenu menu2 : (List<ExtMenu>)object){
					menuBeans2.add(new MenuBean(menu2));
				}
			}
			bean.setChildren(menuBeans2);
			menuBeans.add(bean);
		}
		return menuBeans;
	}
}
