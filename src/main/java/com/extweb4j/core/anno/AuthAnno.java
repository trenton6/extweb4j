package com.extweb4j.core.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 权限验证注解
 * @author gaojun.zhou
 * @date 2016年7月1日
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD})
public @interface AuthAnno {
	
}
