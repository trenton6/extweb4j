package com.extweb4j.core.bean;
/**
 * ext 消息bean
 * @author: 周高军 2016年5月21日
 */
public class Msg {
	private boolean success;
	private String msg;
	private Object ext;
	
	public Msg(boolean success, String msg, Object ext) {
		super();
		this.success = success;
		this.msg = msg;
		this.ext = ext;
	}

	public Msg() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Msg(boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}

	public Msg(boolean success) {
		super();
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getExt() {
		return ext;
	}

	public void setExt(Object ext) {
		this.ext = ext;
	}

}
